provider "aws" {
 region = "us-east-1"
}

module "react-spa-frontend-s3-cf" {
    source                 = "../../modules/services/react-spa-frontend-s3-cf"
    hosted_zone            = var.hosted_zone 
    domain_name            = var.domain_name
    acm_certificate_domain = var.acm_certificate_domain
    zonid                  = var.zonid
    env                    = var.env
    acm_certificate        = var.acm_certificate
    bucket_name            = var.bucket-name-cf
    acl                    = var.acl
    versioning             = var.versioning
    }

