variable "hosted_zone" {
    type = string
}

variable "domain_name" {
    type = string
}

variable "acm_certificate" {
    type = string
}

variable "zonid" {
    type = string
}

variable "acm_certificate_domain" {
    type = string
}

variable "env" {
    type = string
}

variable "bucket-name-cf" {
    type = string
}

variable "acl" {
    type = string
    }

variable "versioning" {
    type = string
}

variable "accountid" {
    type = string
}

variable "aws_region" {
    type = string
}

variable  "cluster_name"  {
    type = string
}

variable  "app_port"  {
    type = string
}

variable  "app_port1"  {
    type = string
}

variable  "fargate_cpu"  {
    type = string
}

variable  "fargate_memory"  {
    type = string
}

variable  "health_check"  {
    type = string
}

variable  "service_name"  {
    type = string
}

variable  "container_name"  {
    type = string
}

variable  "container_port"  {
    type = string
}

variable  "repo-name"  {
    type = string
}

variable  "image_tag_mutability"  {
    type = string
}

variable  "scan_image_on_push"  {
    type = string
}

variable  "desired_count"  {
    type = string
}

variable  "bucket-name-strapi"  {
    type = string
}

variable "project_name" {
    type = string
}

variable "username" {
    type = string
}

variable "password" {
    type = string
}

variable "publicly_accessible" {
    type = string
}

variable "engine" {
    type = string
}

variable "engine-version" {
    type = string
}

variable "port" {
    type = string
}

variable "instance_class" {
    type = string
}

variable "storage_type" {
    type = string
} 

variable "db_name" {
    type = string
}

variable "deletion_protection" {
    type = string
}

variable "allocated_storage" {
    type = string
}

variable "skip_final_snapshot" {
    type = string
}

