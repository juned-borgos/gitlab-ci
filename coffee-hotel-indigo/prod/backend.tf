terraform {
  backend "s3" {
    bucket = "coffeehotel-terraformbackend"
    key    = "terraform/state"
    region = "us-east-1"
  }
}

