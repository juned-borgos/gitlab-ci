hosted_zone             =     "cloudtoday.click"
domain_name             =    "test.cloudtoday.click"
acm_certificate_domain  =    "cloudtoday.click"
zonid                   =    "Z08978411J71TQQD321FO"
env                     =    "prod"
acm_certificate         =    null
bucket-name-cf          =   "coffeehotelindigo-uinew"
acl                     =   "private"
versioning              =   "Disabled"


project_name          =    "coffeehotelindigonew"
accountid             =    "991008360267"
aws_region            =    "us-east-1"
cluster_name          =    "coffeehotelindigo-ecsnew"
app_port              =    "1337"
app_port1             =    "80"
fargate_cpu           =    "2048"
fargate_memory        =    4096
health_check          =    600
service_name          =   "coffeehotelindigo-prod-servicenew"
container_name        =   "strapi-cmsnew"
container_port        =   "1337"
repo-name             =   "coffeehotelindigonew"
image_tag_mutability  =   "MUTABLE"
scan_image_on_push    =   "false"
desired_count         =    "1"
bucket-name-strapi     =    "strapibucket_new"
username              =   "admin"
password              =   "Coffeedevmysql828"
publicly_accessible   =    true
engine               =    "mysql"
engine-version        =   8.0
port                =      "3306"
allocated_storage    =      "20"
storage_type         =     "gp2"
instance_class       =    "db.t3.medium"
db_name              =    "coffeeprodnew"
deletion_protection  =     false
skip_final_snapshot  =     true


