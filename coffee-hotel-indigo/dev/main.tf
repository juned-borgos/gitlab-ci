module "react-spa-frontend-s3-cf" {
    source                 = "../../modules/services/react-spa-frontend-s3-cf"
    hosted_zone            = var.hosted_zone 
    domain_name            = var.domain_name
    acm_certificate_domain = var.acm_certificate_domain
    zonid                  = var.zonid
    env                    = var.env
    acm_certificate        = var.acm_certificate
    bucket_name            = var.bucket-name-cf
    acl                    = var.acl
    versioning             = var.versioning
    }

module "strapi-cms-ecs" {
    source           = "../../modules/services/strapi-cms-ecs/"
    project_name           = var.project_name
    accountid              = var.accountid
    aws_region             = var.aws_region
    cluster_name           = var.cluster_name
    app_port               = var.app_port
    app_port1               = var.app_port1
    fargate_cpu            = var.fargate_cpu
    fargate_memory         = var.fargate_memory
    health_check           = var.health_check
    service_name           = var.service_name
    container_name         = var.container_name
    container_port         = var.container_port
    acm_certificate        = var.acm_certificate
    env                    = var.env
    repo_name              = var.repo-name
    image_tag_mutability   = var.image_tag_mutability
    scan_image_on_push     = var.scan_image_on_push
    desired_count          = var.desired_count
    bucket_name            = var.bucket-name-strapi
    acl                    = var.acl
    versioning             = var.versioning 
    username                = var.username
    password                 = var.password
    publicly_accessible       = var.publicly_accessible
    engine                    = var.engine
    engine-version           = var.engine-version
    port                     = var.port
    allocated_storage       = var.allocated_storage
    storage_type            = var.storage_type
    instance_class          = var.instance_class
    db_name                 =  var.db_name
    deletion_protection      = var.deletion_protection
    skip_final_snapshot      = var.skip_final_snapshot
}


