data "aws_availability_zones" "available" {
}

resource "aws_vpc" "main" {
  cidr_block = var.main_cidr
  enable_dns_hostnames = true 
  enable_dns_support = true 
  tags = {
    Name = "${var.project_name}-vpc"
    project = var.project_name
  }
}

resource "aws_subnet" "private" {
  count             = var.az_count
  cidr_block        = cidrsubnet(aws_vpc.main.cidr_block, 8, count.index)
  availability_zone = data.aws_availability_zones.available.names[count.index]
  vpc_id            = aws_vpc.main.id
  tags = {
    Name = "${var.project_name}-privatesubnet"
    project = var.project_name
  }
}

resource "aws_subnet" "public" {
  count                   = var.az_count
  cidr_block              = cidrsubnet(aws_vpc.main.cidr_block, 8, var.az_count + count.index)
  availability_zone       = data.aws_availability_zones.available.names[count.index]
  vpc_id                  = aws_vpc.main.id
  map_public_ip_on_launch = true
  tags = {
    Name = "${var.project_name}-publicsubnet"
    project = var.project_name
  }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.main.id
  tags = {
    Name = "${var.project_name}-igw"
    project = var.project_name
  }
  
}

resource "aws_route" "internet_access" {

  route_table_id         = aws_vpc.main.main_route_table_id
  destination_cidr_block = var.destination_cidr
  gateway_id             = aws_internet_gateway.gw.id

}

