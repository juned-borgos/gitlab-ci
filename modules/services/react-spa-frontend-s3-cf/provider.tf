provider "aws" {
  region = var.aws_region
  alias  = "acm_provider"
}
